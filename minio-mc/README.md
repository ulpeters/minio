# MioIO Client (mc)

mc is a command to control MinIO. To run mc it within docker:  
- Set your MinIO connection in [.env](.env.template): `cp .env.template .env && vi .env`
- Run the container as one-shot: `docker-compose run minio-mc`
- Background: This will set a your MinIO connection as per [`.env`](.env.template) and make it available under the name `myminio` for the MC CLI running in the [container](https://hub.docker.com/r/minio/mc).


#### Typical commands
```
# buckets
mc mb myminio/documents   # create bucket
mc ls myminio             # list list
mc tree myminio           # list buckets and folders
mc rb myminio/documents   # remove bucket

# dealing with users
mc admin user add myminio johndoe 30RandomAlphaNumericCharacters
mc admin policy set myminio readwrite user=johndoe
mc admin user list myminio
mc admin user info myminio johndoe
mc admin user remove myminio johndoe 

# policies
mc admin policy list myminio 
mc admin policy info readonly
mc admin policy info writeonly
mc admin policy info readwrite
```

#### References
- MC CLI Guides
  - Client: https://docs.min.io/docs/minio-client-complete-guide.html
  - Admin: https://docs.min.io/docs/minio-admin-complete-guide.html
- AWS Policies Documentation (not fully implemented in MinIO)
  - Basics: https://docs.aws.amazon.com/IAM/latest/UserGuide/access_policies_understand.html
  - Reference: https://docs.aws.amazon.com/de_de/IAM/latest/UserGuide/reference_policies_elements.html
  - Amazon Resource Name (ARN) Format: https://docs.aws.amazon.com/general/latest/gr/aws-arns-and-namespaces.html
- Key generation: `apg -M ncl -m 30 -a 0 -n 10`
