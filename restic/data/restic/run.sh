#!/bin/sh -xe

# Init restic repository if neccessary
restic snapshots &>/dev/null || restic init

# Monitoring: Set metric to started
/root/restic/push-metric.sh start

# Run backup job, using parameters from AWS_ACCESS_KEY_ID,
# AWS_SECRET_ACCESS_KEY, RESTIC_PASSWORD, RESTIC_REPOSITORY
restic backup $RESTIC_BACKUP_ARGS $RESTIC_BACKUP_FOLDERS

# Monitoring: Set metric based on the result code of the backup job
case $? in
  0)   /root/restic/push-metric.sh stop ;;
  3)   /root/restic/push-metric.sh incomplete ;;
  1|*) /root/restic/push-metric.sh fail       ;;
esac
  
# Cleanup old backups
restic forget $RESTIC_FORGET_ARGS
