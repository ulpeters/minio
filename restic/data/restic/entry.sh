#!/bin/sh -x

# Import cron file
#/usr/bin/crontab /root/restic/crontab.txt
echo "$CRONTAB" | crontab -

# Start cron
/usr/sbin/crond -f -L /dev/stdout
