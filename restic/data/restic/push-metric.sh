#!/bin/sh -xe

if [ $# -eq 0 ]; then
  echo 'Script to update a Prometheus Pushgateway with restic metrics'
  echo 'Syntax: push-metric.sh [start|stop|incomplete|fail]'
  echo 'Gateway-URL must be set as environment variable in $PROM_GW_URL'
  exit 0
fi

if [ -z $PROM_GW_URL ]; then
  echo 'No Prometheus Push Gateway set in $PROM_GW_URL'
  exit 0 
fi

## Set numeric state value and verbal status based on the received event
case $1 in
  start)      state='1';  status='started'    ;;
  stop)       state='0';  status='stopped'    ;;
  incomplete) state='-3'; status='incomplete' ;;
  fail|*)     state='-1'; status='failed'     ;;
esac

## Update the Prometheus Pushgateway about the state/status
cat <<EOF | curl --data-binary @- $PROM_GW_URL 
# TYPE restic_hook_state gauge
# HELP restic_hook_state 1=started 0=stopped -1=failed -3=incomplete
restic_hook_state $state
# TYPE restic_hook_state_timestamp gauge
restic_hook_state_timestamp{label="$status"} `date +%s`
EOF

## Update the Prometheus Pushgateway about statistics if status is stopped
if [ $status = 'stopped' ]; then
  json=`restic stats --json`

  cat <<EOF | curl --data-binary @- $PROM_GW_URL 
# TYPE restic_repository_total_size_bytes gauge
# HELP restic_repository_total_size_bytes total size of the repository
`echo -n 'restic_repository_total_size_bytes ' && \
echo $json | grep -o '"total_size":[0-9]*' | grep -o '[0-9]*'`

# TYPE restic_repository_total_files_count gauge
# HELP restic_repository_total_files_count total count of files in the repository
`echo -n 'restic_repository_total_files_count ' && \
echo $json | grep -o '"total_file_count":[0-9]*' | grep -o '[0-9]*'`
EOF
fi
