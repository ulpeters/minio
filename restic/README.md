# Restic 

[**Restic**](https://restic.net/), a modern **backup program** which supports many different storage types. 

The setup in this repository will use [**MinIO**](https://github.com/minio/minio), an [Amazon S3](https://aws.amazon.com/s3/) compatible object storage, as **backup target**. Further we'll make use of:
- The official  [restic/restic](https://hub.docker.com/r/restic/restic/) container image
- [Docker Compose](https://docs.docker.com/compose/) for orchistration
- Cron (in the restic container) to schedule backups
- Scripts to run new and clean up old backups
- Scripts to push metrics for [Prometheus](https://prometheus.io/)

## Setup
1. [Get your MinIO server running](https://git.in-ulm.de/ulpeters/minio/) or make use of the [IN-Ulm MinIO Service](https://s3.ulm-store.de/)
2. [Setup a S3 bucket with MinIO MC](https://git.in-ulm.de/ulpeters/minio-mc/)
3. Setup Restic

```
# Setup your config
cp .env.template .env
vi .env

# Run the container
docker-compose up -d
docker logs restic -f
```

## Operation
### Monitoring
For monitoring purposes the script `data/restic/push-metric.sh` is executed prior to and after any backup.
If the environment variable `$PROM_GW_URL` is set, the respective Prometheus Push Gateway will get informed about the backup start and result (stop, incomplete, fail). In case of a proper stop, further statistics will be send to the Push Gateway.

### List snapshots with date/time
```
/ # restic ls
Fatal: Invalid arguments, either give one or more snapshot IDs or set filters.
/ # restic snapshots
repository 276b6a79 opened successfully, password is correct
ID        Time                 Host        Tags        Paths
------------------------------------------------------------------
e70bd81a  2020-01-05 23:39:20  restic                  /mnt/source
d629a178  2020-01-06 20:20:06  restic                  /mnt/source
------------------------------------------------------------------
2 snapshots
```

### Show diff between  snapshots
```
restic diff e70bd81a d629a178
```

## References
- Official Restic Documentation: https://restic.readthedocs.io/en/stable/index.html