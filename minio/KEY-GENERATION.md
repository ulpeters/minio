# Key generation
MinIO will require access and secret keys. To generate a set of random keys to choose from, run:

`docker run -it --rm alpine sh -c 'apk add pwgen && pwgen --secure --capitalize --numerals --ambiguous 30'`

- `--capitalize` Include at least one capital letter in the password
- `--numerals` Include at least one number in the password
- `--symbols` Include at least one special symbol in the password
- `--secure` Generate completely random passwords
- `--ambiguous` Don't include ambiguous characters in the password
- `30` password length

Reference:
[Discussion about key best practices](https://github.com/minio/minio/discussions/15296)


