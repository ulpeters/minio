# MinIO
[**MinIO**](https://github.com/minio/minio), 
an [Amazon S3](https://aws.amazon.com/s3/) compatible object storage server. 
In this repository you'll find everything to get MinIO 
with [Docker Compose](https://docs.docker.com/compose/) going.

## Scope
Scope of the **basic setup** is to run minio  
- as unprivileged user
- with a directoy mounted for data
- with the container port 9000 (S3) mapped to a host port

Scope of the **advance setup** is to run minio in addition
- with TLS support
- with web ui (console, port 9001) exposed
- with TLS support for s3 (port 9000), borrowed from a reverse proxy



## Configuration

```
# Prepare a directory
miniodir="/opt/docker/minio"
[ -d $miniodir ] && echo "Directory $miniodir already exists" && exit 1
mkdir -p /opt/docker/
cd /opt/docker/

# Clone repo
git clone https://git.in-ulm.de/ulpeters/minio.git

# Prepare your configuration
cp .env.template .env
  # Update .env
  # - Set custom access and secret keys
  # - set fqdn in $HOSTNAME
  # - set $CERT_PATH to your reverse proxy
  
  


# Create a home and data directory owned by 1000:1000
mkdir -p $miniodir/data/home $miniodir/data/data
chown -R 1000:1000 $miniodir

# Start miniominio
##basic
docker-compose up -f docker-compose.yml up
docker logs --follow minio

##advances
docker-compose up
docker logs --follow minio
```
- Make sure to [generate](KEY-GENERATION.md) and set custom access and secret keys!!





- [Reverse proxy with Letsencrypt companion](https://git.in-ulm.de/ulpeters/reverse-proxy) running



