# TLS Certificates

## Snake Oil
To get started with snake oil certs:
```
sudo apt install ssl-cert

cp /etc/ssl/certs/ssl-cert-snakeoil.pem ./data/home/.minio/certs/public.crt
sudo cp /etc/ssl/private/ssl-cert-snakeoil.key ./data/home/.minio/certs/private.key
sudo chown -R 1000:1000 data
```

## Certs from proxy-companion 
`docker-compose.overrride.yml` defines a seperate container
to copy certs from another container which runs proxy-companion,
rename them and sets permissions.


### References
  - https://docs.min.io/docs/how-to-secure-access-to-minio-server-with-tls.html
  - https://wiki.ubuntuusers.de/ssl-cert/
